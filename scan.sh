#!/bin/bash

# Find Your IP - A script to find your desktops ip from home
# by Viet&Markus
#
# ex.:
# $ sh test.sh username

### VARIABLES -------------------------------------------------------

USER=$1
BEGIN=2
END=254
ADDRESS='10.49.10.'

### main ------------------------------------------------------------

echo "checking ssh for $ADDRESS$BEGIN to $END"
for i in $(seq $BEGIN $END)
do
  echo  "      $ADDRESS$i "
  # using netcat to check whether the ssh port is open on given address
  if 
  timeout 1 nc -z -v "$ADDRESS$i" 22 2>&1 | grep -q 'succeeded'
  then
    echo " $ADDRESS$i has ssh activated, connecting.."
    ssh -o StrictHostKeyChecking=no -o PasswordAuthentication=no "$USER@$ADDRESS$i"
  fi
done
